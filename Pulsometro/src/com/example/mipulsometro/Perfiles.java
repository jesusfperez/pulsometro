package com.example.mipulsometro;

public class Perfiles {
	
	private String nombre; 
	private String edad;
	private String sexo;


	public Perfiles (String nombre, String edad, String sexo) { 
	    this.nombre = nombre;
	    this.edad=edad;
	    this.sexo=sexo;
	}

	public String get_nombre() { 
	    return nombre; 
	}
	
	public String get_edad() { 
	    return edad; 
	}
 
	public String get_sexo() { 
	    return sexo; 
	}
 
 
}
