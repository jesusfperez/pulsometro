package com.example.mipulsometro;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import com.example.mipulsometro.R;
import com.example.mipulsometro.R.drawable;
import com.example.mipulsometro.R.id;
import com.example.mipulsometro.R.layout;
import com.example.mipulsometro.R.menu;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Medir extends Activity {

	private static TextView numPulsaciones;
	private TextView unidadMedida;
	private TextView txtFecha;
	private Typeface fuente;
	private ActionBar abar;
    private static final AtomicBoolean processing = new AtomicBoolean(false);
    private static SurfaceView preview = null;
    private static SurfaceHolder previewHolder = null;
    private static Camera camera = null;
    private static View imageHeart = null;
    private static WakeLock wakeLock = null;
    private static int averageIndex = 0;
    private static final int averageArraySize = 4;
    private static final int[] averageArray = new int[averageArraySize];
    
    private long startTime2 = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;


    public static enum TYPE {
        GRANDE, PEQUE	
    };

    private static TYPE currentType = TYPE.GRANDE;

    public static TYPE getCurrent() {
        return currentType;
    }

    private static int beatsIndex = 0;
    private static final int beatsArraySize = 3;
    private static final int[] beatsArray = new int[beatsArraySize];
    private static double beats = 0;
    private static long startTime = 0;
    private String resultado;
    private static boolean terminado=false;
    private TextView txtSeg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_medir);
		
		numPulsaciones=(TextView)findViewById(R.id.txtNumPulsaciones);
		unidadMedida=(TextView)findViewById(R.id.txtUnidadMedida);
		fuente= Typeface.createFromAsset(getAssets(), "dsdig.ttf");
		txtFecha=(TextView)findViewById(R.id.txtFecha);
		txtSeg=(TextView)findViewById(R.id.txtSeg);
		
		numPulsaciones.setTypeface(fuente);
		unidadMedida.setTypeface(fuente);
		
		Calendar c=Calendar.getInstance();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
		String fecha=df.format(c.getTime());
		txtFecha.setText(fecha);
		
		preview = (SurfaceView) findViewById(R.id.surfaceView);
		previewHolder = preview.getHolder();
        
        imageHeart = findViewById(R.id.imageHeart);
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
    	previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
       
       
        startTime2 = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);

	}
	private Runnable updateTimerThread = new Runnable() {
		public void run() {
			timeInMilliseconds = SystemClock.uptimeMillis() - startTime2;
			updatedTime = timeSwapBuff + timeInMilliseconds;
			int secs = (int) (updatedTime / 1000);
			
			if(secs<=30)
				txtSeg.setText(secs+"");
			//Numero de segundos de espera
			if(secs==30){
				
				Intent intent= new Intent(Medir.this, Resultado.class);
				intent.putExtra("valor", numPulsaciones.getText()+"");
				String nombre=getIntent().getStringExtra("nick");
				intent.putExtra("nick", nombre);
				camera.stopPreview();
				startActivity(intent);
				timeSwapBuff += timeInMilliseconds;
				customHandler.removeCallbacks(updateTimerThread);
				
			}
			customHandler.postDelayed(this, 0);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getMenuInflater().inflate(R.menu.medir, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		
        switch (item.getItemId()) {
	        case R.id.menu_historial:
	        	
	        	Intent intent = new Intent(Medir.this, Historial.class);
	        	camera.stopPreview();
	   		 	startActivity(intent);
	        	
	            return true;
	  
	        case R.id.menu_ayuda:
	        	
	        	Intent intent4 = new Intent(Medir.this, Ayuda.class);
	        	camera.stopPreview();
	   		 	startActivity(intent4);
	            return true;
	        case R.id.menu_info:
	        	
	        	Intent intent5 = new Intent(Medir.this, Info.class);
	        	camera.stopPreview();
	   		 	startActivity(intent5);
	            return true;
	        default:
	            return super.onContextItemSelected(item);
        }
    }

   @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();

        wakeLock.acquire();
        camera = Camera.open();
        startTime = System.currentTimeMillis();
    }

   @Override
    public void onPause() {
        super.onPause();

        wakeLock.release();
        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    private static PreviewCallback previewCallback = new PreviewCallback() {
    	private int contador=0;
        @Override
        public void onPreviewFrame(byte[] data, Camera cam) {
            if (data == null) throw new NullPointerException();
            Camera.Size size = cam.getParameters().getPreviewSize();
            if (size == null) throw new NullPointerException();

            if (!processing.compareAndSet(false, true)) return;

            int width = size.width;
            int height = size.height;

            int imgAvg = ImageProcessing.decodeYUV420SPtoRedAvg(data.clone(), height, width);
            
            if (imgAvg == 0 || imgAvg == 255) {
                processing.set(false);
                return;
            }

            int averageArrayAvg = 0;
            int averageArrayCnt = 0;
            for (int i = 0; i < averageArray.length; i++) {
                if (averageArray[i] > 0) {
                    averageArrayAvg += averageArray[i];
                    averageArrayCnt++;
                }
            }

            int rollingAverage = (averageArrayCnt > 0) ? (averageArrayAvg / averageArrayCnt) : 0;
            TYPE newType = currentType;
            if (imgAvg < rollingAverage) {
                newType = TYPE.PEQUE;
                if (newType != currentType) {
                    beats++;
                }
            } else if (imgAvg > rollingAverage) {
                newType = TYPE.GRANDE;
            }

            if (averageIndex == averageArraySize) averageIndex = 0;
            averageArray[averageIndex] = imgAvg;
            averageIndex++;

            if (newType != currentType) {
                currentType = newType;
                imageHeart.postInvalidate();
            }

            long endTime = System.currentTimeMillis();
            double totalTimeInSecs = (endTime - startTime) / 1000d;
            //total de seg que actualiza
            if (totalTimeInSecs >= 10) {
                double bps = (beats / totalTimeInSecs);
                int dpm = (int) (bps * 60d);
                if (dpm < 30 || dpm > 180) {
                    startTime = System.currentTimeMillis();
                    beats = 0;
                    processing.set(false);
                    return;
                }

                if (beatsIndex == beatsArraySize) beatsIndex = 0;
                beatsArray[beatsIndex] = dpm;
                beatsIndex++;

                int beatsArrayAvg = 0;
                int beatsArrayCnt = 0;
                for (int i = 0; i < beatsArray.length; i++) {
                    if (beatsArray[i] > 0) {
                        beatsArrayAvg += beatsArray[i];
                        beatsArrayCnt++;
                    }
                }
                int beatsAvg = (beatsArrayAvg / beatsArrayCnt);
                numPulsaciones.setText(String.valueOf(beatsAvg-7));
                startTime = System.currentTimeMillis();
                beats = 0;
            }
            processing.set(false);
            contador+=1;
            
            if(contador >100){
            	
            	terminado=true;
            	return;
            }
        }
    };

    private static SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(previewHolder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback", "Exception in setPreviewDisplay()", t);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            Camera.Size size = getSmallestPreviewSize(width, height, parameters);
            if (size != null) {
                parameters.setPreviewSize(size.width, size.height);
                Log.d("", "Using width=" + size.width + " height=" + size.height);
            }
            camera.setParameters(parameters);
            camera.startPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // Ignore
        }
    };

    private static Camera.Size getSmallestPreviewSize(int width, int height, Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea < resultArea) result = size;
                }
            }
        }
        return result;
    }
}


