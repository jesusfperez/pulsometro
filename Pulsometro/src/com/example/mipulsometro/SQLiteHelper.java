package com.example.mipulsometro;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {
 
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
 
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "usuariosBBDD";
 
    // Table Names
    private static final String TABLE_MEDICIONES = "mediciones";
    private static final String TABLE_PERFILES = "perfiles";
 
 
    // MEDICIONES Table - column nmaes
    private static final String NICK = "nick";
    private static final String FECHA = "fecha";
    private static final String PULSACIONES = "pulsaciones";
    private static final String RESULTADO = "resultado";

 
    // PERFILES Table - column names
    private static final String NICKPER = "nickPer";
    private static final String EDAD = "edad";
    private static final String SEXO = "sexo";

 
    // Table Create Statements
    // Mediciones table create statement
    private static final String CREATE_TABLE_MEDICIONES = "CREATE TABLE "
            + TABLE_MEDICIONES + "(" + NICK + " TEXT," + FECHA
            + " TEXT," + PULSACIONES + " TEXT," + RESULTADO
            + " TEXT" + ")";
 
    // Perfiles table create statement
    private static final String CREATE_TABLE_PERFILES = "CREATE TABLE " + TABLE_PERFILES
            + "(" + NICKPER + " TEXT PRIMARY KEY," + EDAD + " TEXT,"+ SEXO + " TEXT" + ")";
 
    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
 
        // creating required tables
        db.execSQL(CREATE_TABLE_MEDICIONES);
        db.execSQL(CREATE_TABLE_PERFILES);
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICIONES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERFILES);
 
        // create new tables
        onCreate(db);
    }
    
    /*
     * Creating a Mediciones
     */
    public boolean createMediciones(Mediciones m) {
    	boolean valor=false;
        SQLiteDatabase db = this.getWritableDatabase();
     
        ContentValues values = new ContentValues();
        values.put(NICK, m.get_nombre());
        values.put(FECHA, m.get_fecha());
        values.put(PULSACIONES, m.get_pulsaciones());
        values.put(RESULTADO, m.get_resultado());
     
        
        if(db.insert(TABLE_MEDICIONES, null, values)!= -1)
        	valor=true;
        
        return valor;
    }
    
    /*
     * get medicion
     */
    public Mediciones getMedicion(String nick) {
        SQLiteDatabase db = this.getReadableDatabase();
     
        String selectQuery = "SELECT  * FROM " + TABLE_MEDICIONES + " WHERE "
                + NICK + " ='" + nick+"'";
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
     
        
        String nombre=c.getString(c.getColumnIndex(NICK));
        String fecha=c.getString(c.getColumnIndex(FECHA));
        String pulsaciones=c.getString(c.getColumnIndex(PULSACIONES));
        String resultado=c.getString(c.getColumnIndex(RESULTADO));
        
        Mediciones me = new Mediciones(nombre, fecha, pulsaciones, resultado);
     
        return me;
    }
    public ArrayList<Mediciones> getAllMediciones(String nick) {
        ArrayList<Mediciones> todos = new ArrayList<Mediciones>();
        String selectQuery = "SELECT  * FROM " + TABLE_MEDICIONES + " WHERE "
                + NICK + " ='" + nick+"'";
     
        Log.e(LOG, selectQuery);
     
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
     
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                
            	String nombre=c.getString(c.getColumnIndex(NICK));
                String fecha=c.getString(c.getColumnIndex(FECHA));
                String pulsaciones=c.getString(c.getColumnIndex(PULSACIONES));
                String resultado=c.getString(c.getColumnIndex(RESULTADO));
                
                Mediciones me = new Mediciones(nombre, fecha, pulsaciones, resultado);
                // adding to todo list
                todos.add(me);
            } while (c.moveToNext());
        }
     
        return todos;
    }

    /*
     * Deleting a todo
     */
    public boolean deleteMedicion(String nick) {
    	boolean valor=false;
        SQLiteDatabase db = this.getWritableDatabase();
        if(db.delete(TABLE_MEDICIONES, NICK + " = ?",new String[] { String.valueOf(nick) })!=0){
        	valor=true;
        }
        return valor;
    }
   
    /*
     * Creating Perfiles
     */
    public boolean createPerfiles(Perfiles p) {
    	boolean valor=false;
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(NICKPER, p.get_nombre());
        values.put(EDAD, p.get_edad());
        values.put(SEXO, p.get_sexo());
 
        if(db.insert(TABLE_PERFILES, null, values)!= -1)
        	valor=true;
        
        return valor;

    }

    /*
     * get single Perfil
     */
    public Perfiles getPerfil(String nick) {
        SQLiteDatabase db = this.getReadableDatabase();
        
        String selectQuery = "SELECT  * FROM " + TABLE_PERFILES + " WHERE "
                + NICKPER + " ='" + nick+"'";
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
        
        String nombre=c.getString(c.getColumnIndex(NICKPER));
        String edad=c.getString(c.getColumnIndex(EDAD));
        String sexo=c.getString(c.getColumnIndex(SEXO));
        
        Perfiles pe = new Perfiles(nombre, edad, sexo);
     
        return pe;
    }
    /*
     * getting all todos
     * */
    public ArrayList<Perfiles> getAllPerfiles() {
        ArrayList<Perfiles> todos = new ArrayList<Perfiles>();
        String selectQuery = "SELECT  * FROM " + TABLE_PERFILES;
     
        Log.e(LOG, selectQuery);
     
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
     
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                
                String nombre=c.getString(c.getColumnIndex(NICKPER));
                String edad=c.getString(c.getColumnIndex(EDAD));
                String sexo=c.getString(c.getColumnIndex(SEXO));
                
                Perfiles pe = new Perfiles(nombre, edad, sexo);
                // adding to todo list
                todos.add(pe);
            } while (c.moveToNext());
        }
     
        return todos;
    }
    
    /*
     * Deleting a Perfil
     */
    public boolean deletePerfil(String nick) {
    	boolean valor=false;
        SQLiteDatabase db = this.getWritableDatabase();
        if(db.delete(TABLE_PERFILES, NICKPER + " = ?", new String[] { String.valueOf(nick) }) != 0)
        	valor=true;
        db.delete(TABLE_MEDICIONES, NICK + " = ?", new String[] { String.valueOf(nick) });
        return valor;
    }
    /*
     * esta Perfil
     */
    public boolean isPerfil(String nick) {
    	SQLiteDatabase db = this.getReadableDatabase();
    	boolean valor=false;
        
    	String selectQuery = "SELECT  * FROM " + TABLE_PERFILES + " WHERE "
                + NICKPER + " ='" + nick+"'";
     
        Log.e(LOG, selectQuery);
     
        Cursor c = db.rawQuery(selectQuery, null);
     
        if (c != null)
            c.moveToFirst();
        
        String nombre=c.getString(c.getColumnIndex(NICKPER));
        String edad=c.getString(c.getColumnIndex(EDAD));
        String sexo=c.getString(c.getColumnIndex(SEXO));
        
        Perfiles pe = new Perfiles(nombre, edad, sexo);
        
        if(pe!=null)
        	valor=true;
        
        return valor;
    }
 // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
    
}