package com.example.mipulsometro;

import java.util.ArrayList;

import com.example.mipulsometro.R;
import com.example.mipulsometro.R.id;
import com.example.mipulsometro.R.layout;
import com.example.mipulsometro.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CrearPerfil extends Activity {
	private EditText nombre;
	private EditText edad;
	private RadioGroup radioGroupSexo;
	private ActionBar abar;
	String sexoSel;
	private Perfiles p= null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_crear_perfil);
		
		nombre=(EditText)findViewById(R.id.ediTxtNombre);
		edad=(EditText)findViewById(R.id.ediTxtEdad);
		radioGroupSexo=(RadioGroup)findViewById(R.id.radioGroupSexo);
		
		radioGroupSexo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if(checkedId == R.id.radioHom){
					sexoSel="hombre";
				}
				else if(checkedId == R.id.radioMuj){
					sexoSel="mujer";
				}
				
			}
		});
        
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.crear_perfil, menu);
		return true;
	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		
        switch (item.getItemId()) {
	        case R.id.action_guardar:
	        	
	        	SQLiteHelper db = new SQLiteHelper(getApplicationContext());
	        	p= new Perfiles(nombre.getText().toString(), edad.getText().toString(), sexoSel);
	        	
	        	if(db.createPerfiles(p)){
	        		Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "Perfil "+p.get_nombre()+" creado correctamente", Toast.LENGTH_SHORT);
		        	toast1.show();
		        	
		        	Intent intent = new Intent(CrearPerfil.this, Medir.class);
		        	intent.putExtra("nick", nombre.getText()+"");
		   		 	startActivity(intent);
	        	}
	        	else{
	        		Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "Error al crear perfil "+p.get_nombre()+ " ese nick ya existe", Toast.LENGTH_SHORT);
		        	toast1.show();
	        	}
	        	db.closeDB();
	            return true;
	        default:
	            return super.onContextItemSelected(item);
        }
        
    }

}
