package com.example.mipulsometro;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Resultado extends Activity {
	
	private ActionBar abar;
	private TextView txtValor;
	private TextView txtResult;
	private ImageView imgResult;
	private String fecha;
	private String color;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultado);
		
		txtValor= (TextView)findViewById(R.id.txtValor);
		txtResult= (TextView)findViewById(R.id.txtResult);
		imgResult= (ImageView)findViewById(R.id.imgResult);
		
		String valor = getIntent().getStringExtra("valor");
		txtValor.setText(valor);
		
		int numValor=Integer.parseInt(txtValor.getText().toString());
		color = null;
		
		//mirar bbdd para sacar el sexo y edad
		SQLiteHelper db = new SQLiteHelper(getApplicationContext());
		Perfiles p=db.getPerfil(getIntent().getStringExtra("nick"));
		db.close();
		String sexo=p.get_sexo();
		String edad=p.get_edad();
		int numEdad=Integer.parseInt(edad);
		
		if(sexo.equals("hombre")){
			if(numEdad>=20 && numEdad<40){
				if(numValor>84){
					txtResult.setText("Mala");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.rojo);
		            imgResult.setImageDrawable(drawable);
		            color="rojo";
				}
				else if(numValor>=70 && numValor<=84){
					txtResult.setText("Normal");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.amarillo);
		            imgResult.setImageDrawable(drawable);
		            color="amarillo";
				}
				else if(numValor>=60 && numValor<70){
					
		            txtResult.setText("Buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.verde);
		            imgResult.setImageDrawable(drawable);
		            color="verde";
				}
				else if(numValor<60){
					txtResult.setText("Muy buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.azul);
		            imgResult.setImageDrawable(drawable);
		            color="azul";
				}
				
			}
			else if(numEdad>=40 && numEdad<60){
				if(numValor>89){
					txtResult.setText("Mala");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.rojo);
		            imgResult.setImageDrawable(drawable);
		            color="rojo";
				}
				else if(numValor>=74 && numValor<=89){
					txtResult.setText("Normal");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.amarillo);
		            imgResult.setImageDrawable(drawable);
		            color="amarillo";
				}
				else if(numValor>=66 && numValor<74){
					
		            txtResult.setText("Buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.verde);
		            imgResult.setImageDrawable(drawable);
		            color="verde";
				}
				else if(numValor<66){
					txtResult.setText("Muy buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.azul);
		            imgResult.setImageDrawable(drawable);
		            color="azul";
				}	
			}
			else if(numEdad>=60){
				if(numValor>94){
					txtResult.setText("Mala");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.rojo);
		            imgResult.setImageDrawable(drawable);
		            color="rojo";
				}
				else if(numValor>=76 && numValor<=94){
					txtResult.setText("Normal");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.amarillo);
		            imgResult.setImageDrawable(drawable);
		            color="amarillo";
				}
				else if(numValor>=68 && numValor<76){
					
		            txtResult.setText("Buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.verde);
		            imgResult.setImageDrawable(drawable);
		            color="verde";
				}
				else if(numValor<68){
					txtResult.setText("Muy buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.azul);
		            imgResult.setImageDrawable(drawable);
		            color="azul";
				}
			}
		}
		else{
			if(numEdad>=20 && numEdad<40){
				if(numValor>98){
					txtResult.setText("Mala");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.rojo);
		            imgResult.setImageDrawable(drawable);
		            color="rojo";
				}
				else if(numValor>=80 && numValor<=98){
					txtResult.setText("Normal");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.amarillo);
		            imgResult.setImageDrawable(drawable);
		            color="amarillo";
				}
				else if(numValor>=70 && numValor<80){
					
		            txtResult.setText("Buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.verde);
		            imgResult.setImageDrawable(drawable);
		            color="verde";
				}
				else if(numValor<70){
					txtResult.setText("Muy buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.azul);
		            imgResult.setImageDrawable(drawable);
		            color="azul";
				}
				
			}
			else if(numEdad>=40 && numEdad<60){
				if(numValor>102){
					txtResult.setText("Mala");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.rojo);
		            imgResult.setImageDrawable(drawable);
		            color="rojo";
				}
				else if(numValor>=82 && numValor<=102){
					txtResult.setText("Normal");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.amarillo);
		            imgResult.setImageDrawable(drawable);
		            color="amarillo";
				}
				else if(numValor>=74 && numValor<82){
					
		            txtResult.setText("Buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.verde);
		            imgResult.setImageDrawable(drawable);
		            color="verde";
				}
				else if(numValor<74){
					txtResult.setText("Muy buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.azul);
		            imgResult.setImageDrawable(drawable);
		            color="azul";
				}	
			}
			else if(numEdad>=60){
				if(numValor>108){
					txtResult.setText("Mala");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.rojo);
		            imgResult.setImageDrawable(drawable);
		            color="rojo";
				}
				else if(numValor>=88 && numValor<=108){
					txtResult.setText("Normal");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.amarillo);
		            imgResult.setImageDrawable(drawable);
		            color="amarillo";
				}
				else if(numValor>=78 && numValor<88){
					
		            txtResult.setText("Buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.verde);
		            imgResult.setImageDrawable(drawable);
		            color="verde";
				}
				else if(numValor<78){
					txtResult.setText("Muy buena");
					Resources res = getResources();
		            Drawable drawable = res.getDrawable(R.drawable.azul);
		            imgResult.setImageDrawable(drawable);
		            color="azul";
				}
			}
		}
		Calendar c=Calendar.getInstance();
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
	    fecha=df.format(c.getTime());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resultado, menu);
		return true;
	}
	public void Compartir(){
		   Intent intentCompartir = new Intent(Intent.ACTION_SEND);
		   intentCompartir.setType("text/plain");
		   intentCompartir.putExtra(Intent.EXTRA_SUBJECT, "Pulsaciones "+getIntent().getStringExtra("nick"));
		   intentCompartir.putExtra(Intent.EXTRA_TEXT, "Las pulsaciones de "+getIntent().getStringExtra("nick")+
				   " a fecha de "+fecha+" son "+txtValor.getText().toString()+" ppm");

		   startActivity(Intent.createChooser(intentCompartir , "T�tulo de la Ventana"));
		}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		
        switch (item.getItemId()) {
	        case R.id.action_guardar:
	        	
	        	SQLiteHelper db = new SQLiteHelper(getApplicationContext());
	        	Mediciones m= new Mediciones(getIntent().getStringExtra("nick"), fecha, txtValor.getText().toString()+" ppm", color);
				if(db.createMediciones(m)){
					Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "Medici�n guardada correctamente", Toast.LENGTH_SHORT);
		        	toast1.show();
		        	Intent intent = new Intent(Resultado.this, Historial.class);
					intent.putExtra("nick", m.get_nombre()+"");
					startActivity(intent);
				}
				else{
					Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "No se ha guardado la medici�n", Toast.LENGTH_SHORT);
		        	toast1.show();
				}
				db.closeDB();
	            return true;
	        case R.id.menu_social:
	        	Compartir();
	        	
	            return true;
	  
	        default:
	            return super.onContextItemSelected(item);
        }
        
    }

}
