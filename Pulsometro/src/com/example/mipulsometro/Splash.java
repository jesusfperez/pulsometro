package com.example.mipulsometro;

import java.util.Timer;
import java.util.TimerTask;

import com.example.mipulsometro.R;
import com.example.mipulsometro.R.layout;
import com.example.mipulsometro.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class Splash extends Activity {

	private long splashDelay = 3000; //3 segundos
	private ActionBar abar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar = getActionBar();
		abar.hide();
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_splash);
	    
	    TimerTask task = new TimerTask() {
        @Override
        public void run() {
	        Intent mainIntent = new Intent().setClass(Splash.this, Loging.class);
	        startActivity(mainIntent);
	        finish();//Destruimos esta activity para prevenir que el usuario retorne aqui presionando el boton Atras.
		  }
		};

		Timer timer = new Timer();
		timer.schedule(task, splashDelay);//Pasado los 3 segundos dispara la tarea
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

}
