package com.example.mipulsometro;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MostrarHist extends Activity {
	private ActionBar abar;
	private ListView lstMediciones;
	private ArrayList<Mediciones> mediciones= new ArrayList<Mediciones>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostar_hist);
		
		//BBDD
		SQLiteHelper db = new SQLiteHelper(getApplicationContext());
		String nick=getIntent().getStringExtra("nick");
		mediciones=db.getAllMediciones(nick);
		db.closeDB();
		
		lstMediciones = (ListView) findViewById(R.id.lstMediciones);
        lstMediciones.setAdapter(new AdaptadorMediciones(this, R.layout.adaptador_mediciones, mediciones) {
			 
			@Override
			public void onEntrada(Object entrada, View view) {
				
				TextView txtNickMed=(TextView) view.findViewById(R.id.txtIdent); 
				TextView txtFechaMed=(TextView) view.findViewById(R.id.txtFechaMedicion); 
				TextView txtPulsacionesMed=(TextView) view.findViewById(R.id.txtPulsacionesMed); 
				ImageView imgResultMed=(ImageView)view.findViewById(R.id.imgResulMed); 
				
	            txtNickMed.setText(((Mediciones) entrada).get_nombre()); 
	            txtFechaMed.setText(((Mediciones) entrada).get_fecha()); 
	            txtPulsacionesMed.setText(((Mediciones) entrada).get_pulsaciones()); 
	            String color=((Mediciones) entrada).get_resultado();
	            
	            if(color.equals("verde")) {
	            	Resources res = getResources();
	                Drawable drawable = res.getDrawable(R.drawable.verde);
	                imgResultMed.setImageDrawable(drawable);
				}
	            else if(color.equals("amarillo")){
	            	Resources res = getResources();
	                Drawable drawable = res.getDrawable(R.drawable.amarillo);
	                imgResultMed.setImageDrawable(drawable);
	            }
	            else if(color.equals("azul")){
	            	Resources res = getResources();
	                Drawable drawable = res.getDrawable(R.drawable.azul);
	                imgResultMed.setImageDrawable(drawable);
	            }
	            else if (color.equals("rojo")){
	            	Resources res = getResources();
	                Drawable drawable = res.getDrawable(R.drawable.rojo);
	                imgResultMed.setImageDrawable(drawable);
	            }
			}
		});
	}
}
