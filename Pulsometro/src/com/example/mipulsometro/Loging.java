package com.example.mipulsometro;

import com.example.mipulsometro.R;
import com.example.mipulsometro.R.id;
import com.example.mipulsometro.R.layout;
import com.example.mipulsometro.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Loging extends Activity {
	
	private ActionBar abar;
	private EditText editNick;
	private Button btnEntrar;
	private Button btnCrearPerf;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loging);
		
		editNick=(EditText)findViewById(R.id.editNick);
		btnEntrar=(Button)findViewById(R.id.btnEntrar);
		btnCrearPerf=(Button)findViewById(R.id.btnCrearPerf);
		
		btnEntrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
	   		 	SQLiteHelper db = new SQLiteHelper(getApplicationContext());
	   		 	if(db.isPerfil(editNick.getText().toString())){
		   		 	Intent intent2 = new Intent(Loging.this, Medir.class);
		   		 	intent2.putExtra("nick", editNick.getText().toString());
		   		 	startActivity(intent2);
	   		 	}
	   		 	else{
		   		 	Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "El nick "+editNick.getText().toString()+" no existe en bbdd", Toast.LENGTH_SHORT);
		        	toast1.show();	
	   		 	}
		 		db.closeDB();
			}
		});
		
		btnCrearPerf.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(Loging.this, CrearPerfil.class);
	   		 	startActivity(intent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.loging, menu);
		return true;
	}

}
