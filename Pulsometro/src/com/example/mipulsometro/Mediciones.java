package com.example.mipulsometro;

public class Mediciones {
	
	private String nombre; 
	private String fecha;
	private String pulsaciones;
	private String resultado;


	public Mediciones (String nombre, String fecha, String pulsaciones, String resultado) { 
	    this.nombre = nombre;
	    this.fecha=fecha;
	    this.pulsaciones=pulsaciones;
	    this.resultado=resultado;
	}

	public String get_nombre() { 
	    return nombre; 
	}
	
	public String get_fecha() { 
	    return fecha; 
	}
 
	public String get_pulsaciones() { 
	    return pulsaciones; 
	}
 
	public String get_resultado() { 
	    return resultado; 
	}
 
 
}