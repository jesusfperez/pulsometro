package com.example.mipulsometro;

import com.example.mipulsometro.R;
import com.example.mipulsometro.R.layout;
import com.example.mipulsometro.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;

public class Info extends Activity {
	private ActionBar abar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.info, menu);
		return true;
	}

}
