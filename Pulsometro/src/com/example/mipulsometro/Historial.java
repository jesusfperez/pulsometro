package com.example.mipulsometro;

import java.util.ArrayList;

import com.example.mipulsometro.R;
import com.example.mipulsometro.R.id;
import com.example.mipulsometro.R.layout;
import com.example.mipulsometro.R.menu;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Historial extends Activity {

	private ListView lstPersonas;
	private ActionBar abar;
	private TextView txtNombre;
	private ArrayList<Perfiles> nombres= new ArrayList<Perfiles>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		abar= getActionBar();
		abar.setDisplayShowTitleEnabled(false);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_historial);
		//BBDD
		SQLiteHelper db = new SQLiteHelper(getApplicationContext());
		nombres=db.getAllPerfiles();
		db.closeDB();
		
		lstPersonas = (ListView) findViewById(R.id.lstPersonas);
        lstPersonas.setAdapter(new AdaptadorPerfilesHistorial(this, R.layout.adaptador_perfiles_historial, nombres) {
			 
			@Override
			public void onEntrada(Object entrada, View view) {
				
				txtNombre = (TextView) view.findViewById(R.id.txtNombrePer); 
	            txtNombre.setText(((Perfiles) entrada).get_nombre()); 
			}
		});
        lstPersonas.setOnItemClickListener(new OnItemClickListener() { 
			@Override
			public void onItemClick(AdapterView<?> pariente, View view, int posicion, long id) {
				Intent intent = new Intent(Historial.this, MostrarHist.class);
				Perfiles p= (Perfiles) lstPersonas.getItemAtPosition(posicion);
				intent.putExtra("nick", p.get_nombre());
	   		 	startActivity(intent);
			}
        });	
        registerForContextMenu(lstPersonas);
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo){
	    super.onCreateContextMenu(menu, v, menuInfo);
	 
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.historial, menu);
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	 
	    switch (item.getItemId()) {
	        case R.id.eliminar:
	        	
	        	SQLiteHelper db = new SQLiteHelper(getApplicationContext());
	        	if(db.deletePerfil(txtNombre.getText().toString())){
	        		Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "Eliminando perfil.."+txtNombre.getText().toString(), Toast.LENGTH_SHORT);
		        	toast1.show();
		        	nombres=db.getAllPerfiles();
		        	//para actualizar la activity
		    		finish();
		    		startActivity(getIntent());
	        	}
	        	else{
	        		Toast toast1 =Toast.makeText(getApplicationContext(),
		                    "No se ha podido eliminar el perfil"+txtNombre.getText().toString(), Toast.LENGTH_SHORT);
		        	toast1.show();
	        	}
	    		db.closeDB();
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}

}
